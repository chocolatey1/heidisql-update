#!/usr/bin/env python3
"""
HeidiSQL_to_choco.py

- Find the most recent release on the HeidiSQL website
- If there is a new release:
    - Update the HeidiSQL package source
    - git commit (but don't push)
    - Create updated package (choco pack)
    - Push the new package to the Chocolatey community repository

Usage:
  HeidiSQL_to_choco.py [ --config=<config_file> ]
  HeidiSQL_to_choco.py -h | --help

Options:
  --ini=<ini_file> Configuration file
                   (default: HeidiSQL_to_choco.ini in same directory as HeidiSQL_update.py)
  -h, --help       Show this help

Configuration file:

"""

import configparser
import dataclasses
import re
import subprocess
import sys
import xml.etree.ElementTree as ET
from pathlib import Path

import docopt
import requests


SCRIPT_DIR = Path(sys.argv[0]).parent
DIST_DIR = SCRIPT_DIR / 'dist'


@dataclasses.dataclass
class Config:
    package_source_dir: Path
    push_url: str
    api_key: str

    @classmethod
    def read_from_ini(cls, ini_path: Path) -> 'Config':
        cp = configparser.ConfigParser()
        with ini_path.open('rt') as f:
            cp.read_file(f)
        return Config(
            package_source_dir=Path(cp['general']['package_source_dir']).resolve(),
            push_url=cp['general']['push_url'],
            api_key=cp['general']['api_key'],
        )


@dataclasses.dataclass(order=True)
class Version:
    major: int
    minor: int
    build: int
    revision: int
    
    @classmethod
    def from_string(cls, s: str) -> 'Version':
        major, minor, build, revision = s.split('.')
        major, minor, build, revision = map(int, (major, minor, build, revision))
        return cls(major, minor, build, revision)

    def __str__(self):
        return f'{self.major}.{self.minor}.{self.build}.{self.revision}'


@dataclasses.dataclass
class ReleaseInfo:
    version: Version
    url: str
    checksum: str
    checksum_type: str


def read_version_from_local_package(package_source_dir: Path) -> Version:
    ns_aliases = {'nuspec': 'http://schemas.microsoft.com/packaging/2011/08/nuspec.xsd' }
    tree = ET.parse(package_source_dir / 'src' / 'heidisql.nuspec')
    package_elem = tree.getroot()
    version_elem = package_elem.find('./nuspec:metadata/nuspec:version', ns_aliases)
    return Version.from_string(version_elem.text)


def read_release_from_website() -> ReleaseInfo:
    base_url = 'https://www.heidisql.com'
    download_page_url = base_url + '/download.php'
    r = requests.get(download_page_url)
    r.raise_for_status()
    # Use re to extract info from the html. Blasphemy! It's only for internal use though,
    # it'll work for now. Feel free to change to a better method.
    mo = re.search(r'\<a href="(/installers/HeidiSQL_(.+)_Setup\.exe)"', r.text)
    if not mo:
        raise RuntimeError('unable to find the download link')
    download_link = base_url + mo.group(1)
    version = Version.from_string(mo.group(2))
    mo = re.search(r'\<a class="checksum" title="SHA1 checksum" href="(/installers/HeidiSQL_.+_Setup.sha1.txt)"\>&TildeEqual;\</a\>', r.text)
    if not mo:
        raise RuntimeError('unable to find the checksum link')
    sha1_checksum_link = mo.group(1)
    sha1_checksum = requests.get(base_url + sha1_checksum_link).text
    return ReleaseInfo(
        version=version,
        url=download_link,
        checksum=sha1_checksum,
        checksum_type='sha1',
    )


def find_choco_version() -> Version:
    p = subprocess.run(['choco', 'find', '--nocolor', '--exact', 'HeidiSQL'], capture_output=True)
    # Output looks like:
    #   Chocolatey v2.2.2
    #   HeidiSQL 12.6.0.6765 [Approved] Downloads cached for licensed users
    #   1 packages found.
    # Output encoding: perhaps UTF-8 or Latin-1 or Windows-1251 or who knows, but I don't
    # really expect any non-ASCII characters so ASCII should do
    output = p.stdout.decode('ascii')
    heidi_line, *_ = [line for line in output.splitlines(keepends=False) if line.lower().startswith('heidisql')]
    _, version_string, *_ = heidi_line.split()
    version = Version.from_string(version_string)
    return version


def update_nuspec(fn: Path, release_info: ReleaseInfo) -> None:
    print(f'    {fn!s}')
    old_lines = fn.read_text().splitlines(keepends=True)
    new_lines = []
    for line in old_lines:
        if line.startswith('    <version>'):
            line = f'    <version>{release_info.version!s}</version>\n'
        new_lines.append(line)
    fn.write_text(''.join(new_lines))


def update_install_script(fn: Path, release_info: ReleaseInfo) -> None:
    print(f'    {fn!s}')
    old_lines = fn.read_text().splitlines(keepends=True)
    new_lines = []
    for line in old_lines:
        if line.startswith('$url        ='):
            line = f"$url        = '{release_info.url}'\n"
        elif line.startswith('  checksum      = '):
            line = f"  checksum      = '{release_info.checksum}'\n"
        elif line.startswith('  checksumType  ='):
            line = f"  checksumType  = '{release_info.checksum_type}' #default is md5, can also be sha1\n"
        new_lines.append(line)
    fn.write_text(''.join(new_lines))


def update_package_source_files(package_source_dir: Path, release_info: ReleaseInfo) -> None:
    print('update source file with info from new release:')
    update_nuspec(package_source_dir / 'src' / 'heidisql.nuspec', release_info)
    update_install_script(package_source_dir / 'src' / 'tools' / 'chocolateyinstall.ps1', release_info)


def git_commit(package_source_dir: Path, release_info: ReleaseInfo) -> None:
    print('git commit updated source files')
    subprocess.run(
        [
            'git',
            'commit',
            '--all',
            '--message', f'Update package to version {release_info.version!s}',
        ],
        cwd=package_source_dir,
    )


def choco_pack(package_source_dir: Path, dist_dir: Path) -> Path:
    print(f'choco pack package into {dist_dir!s}')
    p = subprocess.run(
        [
            'choco',
            'pack',
            '--no-color',
            str(package_source_dir / 'src' / 'heidisql.nuspec'),
            '--outputdirectory', str(dist_dir),
        ],
        capture_output=True,
    )
    # Output looks like:
    #   Chocolatey v2.2.2
    #   Attempting to build package from 'heidisql.nuspec'.
    #   Successfully created package 'C:\Dev\Personal\chocolatey\heidisql-update\heidisql.12.7.0.6850.nupkg'
    output = p.stdout.decode('ascii')
    fn = re.search(r'heidisql[^\\]*\.nupkg', output).group(0)
    return Path(dist_dir) / fn


def choco_push(pkg: Path, push_url: str, api_key: str) -> None:
    # choco push packageName.nupkg --source "'https://push.chocolatey.org/'"
    print(f'choco push package to {push_url}:')
    subprocess.run(
        [
            'choco', 
            'push', 
            str(pkg), 
            '--source', f'"{push_url}"', 
            '--api-key', f'"{api_key}"',
        ],
    )


def main(config: Config) -> None:
    local_package_version = read_version_from_local_package(config.package_source_dir)
    print(f'local version: {local_package_version}')
    available_release_info = read_release_from_website()
    print(f'available version: {available_release_info.version}')
    choco_version = find_choco_version()
    print(f'choco version: {choco_version!s}')
    print()

    if available_release_info.version < local_package_version:
        raise RuntimeError(f'released version ({available_release_info.version}) < local package version ({local_package_version})! Check what is going on!')
    if available_release_info.version > local_package_version:
        update_package_source_files(config.package_source_dir, available_release_info)
        print()

    git_commit(config.package_source_dir, available_release_info)
    print()

    if available_release_info.version > choco_version:
        DIST_DIR.mkdir(exist_ok=True)
        pkg = choco_pack(config.package_source_dir, DIST_DIR)
        choco_push(pkg, config.push_url, config.api_key)


if __name__ == '__main__':
    args = docopt.docopt(__doc__)
    config_fn = args.get('--config')
    if config_fn:
        config_fn = Path(config_fn).resolve()
    else:
        config_fn = (SCRIPT_DIR / 'HeidiSQL_to_choco.ini').resolve()
    config = Config.read_from_ini(config_fn)
    main(config)
