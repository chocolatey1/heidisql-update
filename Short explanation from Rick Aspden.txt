As for pushing up to Chocolatey, it's really easy. You need to grab an API key
for https://community.chocolatey.org/account and configure Chocolatey with it
for the community server - e.g.

    choco apikey add --source "'https://push.chocolatey.org/'" --key "'API_KEY_HERE'"
    
(see documentation here - https://docs.chocolatey.org/en-us/create/create-packages#push-your-package ),
after updating the SHA1 hash and the installer exe from the HeidiSQL site
(there's a link with the symbol ≃ for the hash next to the installer download
on the HeidiSQL site that the maintainer put up for me to help with the
Chocolatey package).